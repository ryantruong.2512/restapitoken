﻿using Microsoft.AspNetCore.Http;
using SemiWebAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Middlewares
{
    public class SimpleMiddleware
    {
        UserService _userSvc;
        private readonly RequestDelegate _next;

        public SimpleMiddleware(RequestDelegate next, UserService userSvc)
        {
            _userSvc = userSvc;
            _next = next;
        }

        public async System.Threading.Tasks.Task Invoke(HttpContext context)
        {
            if(!String.IsNullOrEmpty(context.Request.Headers["Authorization"]))
            {
                var access_token = context.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
                var user = _userSvc.FindByAccessTokenAsync(access_token);
                if (user is null)
                {
                    context.Response.StatusCode = 404;
                    await context.Response.WriteAsync("Token Error!");
                }
            }
            await _next(context);
            //await context.Response.WriteAsync("Token Error!");
            //await _next(context);
            //await context.Response.WriteAsync("<div> Bye from Simple Middleware </div>");
        }
    }
}
