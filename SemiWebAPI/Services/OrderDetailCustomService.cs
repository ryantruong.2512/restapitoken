﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class OrderDetailCustomService
    {
        OrderDetailCustomSQL _campcustom;
        public OrderDetailCustomService(IMySQLSetting setting)
        {
            _campcustom = new OrderDetailCustomSQL(setting);
        }
        public OrderDetailCustom Create(OrderDetailCustom obj)
        {
            _campcustom.Insert(obj);
            return obj;
        }
        public IList<OrderDetailCustom> Read() =>
            _campcustom.FindAll();

        public OrderDetailCustom Find(string id) =>
            _campcustom.Find(id);

        public OrderDetailCustom Update(OrderDetailCustom obj)
        {
            var result = _campcustom.Update(obj);
            return result?obj:null;
        }

        public string Delete(string id)
        {
            return _campcustom.Delete(id)?id:null;
            
        }

    }
}
