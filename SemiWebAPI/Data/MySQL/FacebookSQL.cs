﻿//using MySql.Data.MySqlClient;
using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class FacebookSQL : MySQLAccess
    {
        public FacebookSQL(IMySQLSetting _mysql) :base(_mysql)
        {

        }
        public List<Facebook_Account> FindAll()
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `email`, `token`, `status`, `log`,`id` FROM `facebook_accounts` WHERE 1";
                var accounts = new List<Facebook_Account>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Facebook_Account()
                        {
                            
                            email = reader.GetFieldValue<string>(0),
                            token = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            log = reader.GetFieldValue<string>(3),
                            id = reader.GetFieldValue<int>(4).ToString()
                        };
                        accounts.Add(t);
                    }
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public Facebook_Account Find(string email)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `email`, `token`, `status`, `log` FROM `facebook_accounts`,`id` WHERE `email` = @email";
                cmd.Parameters.AddWithValue("@email", email);
                var accounts = new List<Facebook_Account>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Facebook_Account()
                        {
                            email = reader.GetFieldValue<string>(0),
                            token = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            log = reader.GetFieldValue<string>(3),
                            id = reader.GetFieldValue<string>(4)
                        };
                        accounts.Add(t);
                    }
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(Facebook_Account account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `facebook_accounts`(`email`, `token`, `status`) VALUES (@email,@token,@status)";
                cmd.Parameters.AddWithValue("@email", account.email);
                cmd.Parameters.AddWithValue("@token", account.token);
                cmd.Parameters.AddWithValue("@status", account.status);
                cmd.Parameters.AddWithValue("@log", account.log==null?"":account.log);

                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(Facebook_Account account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `facebook_accounts` SET `token`=@token,`status`=@status,`log`=@log ,`last_date_campaign`=@last_date_campaign,`last_date_crawl`=@last_date_crawl WHERE `email` = @email";
                cmd.Parameters.AddWithValue("@email", account.email);
                cmd.Parameters.AddWithValue("@token", account.token);
                cmd.Parameters.AddWithValue("@status", account.status);
                if (account.log == null)
                {
                    cmd.CommandText = cmd.CommandText.Replace("`log`=@log", "`log`=`log`");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@log", account.log);
                }
                if (account.last_date_campaign == null)
                {
                    cmd.CommandText = cmd.CommandText.Replace("`last_date_campaign`=@last_date_campaign", "`last_date_campaign`=`last_date_campaign`");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@last_date_campaign", account.last_date_campaign);
                }
                if (account.last_date_crawl == null)
                {
                    cmd.CommandText = cmd.CommandText.Replace("`last_date_crawl`=@last_date_crawl", "`last_date_crawl`=`last_date_crawl`");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@last_date_crawl", account.last_date_crawl);
                }
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string email)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `facebook_accounts` WHERE `email` = @email";
                cmd.Parameters.AddWithValue("@email", email);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }

   
    }
}
