﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Services;
using SemiWebAPI.Models;
using SemiWebAPI.Provider;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        UserService _userSvc;
        private readonly ActionExecutedContext context;

        public UsersController(UserService userService)
        {
            _userSvc = userService;
        }
        [HttpPost("login")]
        public ActionResult Login([FromBody] User user)
        {
            var _user = _userSvc.Login(user.username, user.password);
            if (_user != null)
            {
                var access_token = (new AuthProvider()).GenerateToken(_user);
                _user.access_token = access_token;
                _userSvc.UpdateToken(_user);
                return Ok(access_token);
            }
            return Ok("Error!");
        }
        [HttpGet("check")]
        public ActionResult Check()
        {
            return Ok(_userSvc.Check());
        }
        [HttpGet("me")]
        public ActionResult GetMe()
        {
            var token = Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            var user = _userSvc.FindByAccessTokenAsync(token);
            return Ok(user);
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_userSvc.Read());
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_userSvc.Find(id.ToString()));
        }

      
        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] User user)
        {
            try
            {
                return Ok(_userSvc.Create(user));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] User user)
        {
            try
            {
                return Ok(_userSvc.Update(user));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_userSvc.Delete(id));
        }
    }
}
