﻿using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    class ShopbaseSQL : MySQLAccess
    {
        public ShopbaseSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }
        public List<Shopbase_Account> FindAll()        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `store`, `api_url`, `username`, `password`, `status`, `id` FROM `shopbase_accounts` WHERE 1";
                var accounts = new List<Shopbase_Account>();

                using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                        var t = new Shopbase_Account()
                        {
                            store = reader.GetFieldValue<string>(0),
                            api_url = reader.GetFieldValue<string>(1),
                            username = reader.GetFieldValue<string>(2),
                            password = reader.GetFieldValue<string>(3),
                            status = reader.GetFieldValue<string>(4),
                            id = reader.GetFieldValue<int>(5).ToString()
                    };
                    accounts.Add(t);
                }
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public Shopbase_Account Find(string id)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `store`, `api_url`, `username`, `password`, `status`, `id` FROM `shopbase_accounts` WHERE `id` = @id";
                var accounts = new List<Shopbase_Account>();
                cmd.Parameters.AddWithValue("@id", id);
                using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    var t = new Shopbase_Account()
                    {
                        store = reader.GetFieldValue<string>(0),
                        api_url = reader.GetFieldValue<string>(1),
                        username = reader.GetFieldValue<string>(2),
                        password = reader.GetFieldValue<string>(3),
                        status = reader.GetFieldValue<string>(4),
                        id = reader.GetFieldValue<int>(5).ToString()
                    };
                    accounts.Add(t);
                }
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(Shopbase_Account account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `shopbase_accounts`(`store`, `api_url`, `username`, `password`, `status`) VALUES (@store,@api_url,@username,@password,@status)";
                cmd.Parameters.AddWithValue("@store", account.store);
                cmd.Parameters.AddWithValue("@api_url", account.api_url);
                cmd.Parameters.AddWithValue("@username", account.username);
                cmd.Parameters.AddWithValue("@password", account.password);
                cmd.Parameters.AddWithValue("@status", account.status);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(Shopbase_Account account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `shopbase_accounts` SET `api_url`=@api_url, `username`=@username,`password`=@password,`status`=@status,`log`=@log,`last_date_order`=@last_date_order,`last_date_crawl`=@last_date_crawl WHERE `id`=@id";
                cmd.Parameters.AddWithValue("@id", account.id);
                cmd.Parameters.AddWithValue("@api_url", account.api_url);
                cmd.Parameters.AddWithValue("@username", account.username);
                cmd.Parameters.AddWithValue("@password", account.password);
                cmd.Parameters.AddWithValue("@status", account.status);
                if (account.log == null)
                {
                    cmd.CommandText = cmd.CommandText.Replace("`log`=@log", "`log`=`log`");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@log", account.log);
                }
                if (account.last_date_order == null)
                {
                    cmd.CommandText = cmd.CommandText.Replace("`last_date_order`=@last_date_order", "`last_date_order`=`last_date_order`");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@last_date_order", account.last_date_order);
                }

                if (account.last_date_crawl == null)
                {
                    cmd.CommandText = cmd.CommandText.Replace("`last_date_crawl`=@last_date_crawl", "`last_date_crawl`=`last_date_crawl`");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@last_date_crawl", account.last_date_crawl);
                }
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `shopbase_accounts` WHERE `id` =@id";
                cmd.Parameters.AddWithValue("@id", id);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
    }
}
