﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public interface IMySQLSetting
    {
        string ConnectionString { get; set; }
    }

    public class MySQLSetting : IMySQLSetting
    {
        public string ConnectionString { get; set; }
    }
}
