﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    [BsonIgnoreExtraElements]
    public class Store
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string storename { get; set; }
        public string api_url { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
