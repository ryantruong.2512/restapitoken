﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Models;
namespace SemiWebAPI.Services
{
    public class StoresService
    {
        private readonly IMongoCollection<Store> _stores;

        public StoresService(IDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _stores = database.GetCollection<Store>("stores");
        }
        public Store Create(Store store)
        {
            _stores.InsertOne(store);
            return store;
        }
        public IList<Store> Read() =>
            _stores.Find(sub => true).ToList();

        public Store Find(string id) =>
            _stores.Find(sub => sub.Id == id).SingleOrDefault();

        public Store Update(Store store){
            var result = _stores.ReplaceOne(sub => sub.Id == store.Id, store).UpsertedId;
            if(result != null)
            {
                return store;
            }
            return null;
        }

        public string Delete(string id)
        {
            var result = _stores.DeleteOne(sub => sub.Id == id).DeletedCount;
            if(result>0)
            {
                return id;
            }
            return null;
        }
           
    }
}
