﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class SellerService
    {
        SellerSQL _ssql;
        public SellerService(IMySQLSetting setting)
        {
            _ssql = new SellerSQL(setting);
        }
        public Seller Create(Seller account)
        {
            _ssql.Insert(account);
            return account;
        }
        public int Import(List<Seller> Sellers)
        {
            _ssql.DeleteAll();
            int count = 0;
            foreach(var Seller in Sellers)
            {
                if (_ssql.Insert(Seller))
                    count++;
            }
            return count;
        }
        public IList<Seller> Read() =>
            _ssql.FindAll();

        public Seller Find(string id) =>
            _ssql.Find(id);

        public Seller Update(Seller obj)
        {
            var result = _ssql.Update(obj);
            return result ? obj : null;
        }

        public string Delete(string id)
        {
            return _ssql.Delete(id) ? id : null;

        }
    }
}
