﻿//using MySql.Data.MySqlClient;
using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class OrderDetailCustomSQL : MySQLAccess
    {
        public OrderDetailCustomSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }
        public List<OrderDetailCustom> FindAll()
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `orderid`, `store`, `order_nbr`, `product_id`, `product_sku`, `product_url`, `qty`, `price`, `total_discount`, `date`, `created_at`, `updated_at` FROM `order_detail_custom` WHERE 1";
                var accounts = new List<OrderDetailCustom>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new OrderDetailCustom()
                        {
                           id = reader.GetFieldValue<int>(0),
                            orderid = reader.GetFieldValue<string>(1),
                            store = reader.GetFieldValue<string>(3),
                            order_nbr = reader.GetFieldValue<string>(3),
                            product_id = reader.GetFieldValue<string>(4),
                            product_sku = reader.GetFieldValue<string>(5),
                            product_url = reader.GetFieldValue<string>(6),
                            qty = reader.GetFieldValue<int>(7),
                            price = reader.GetFieldValue<double>(8),
                            total_discount = reader.GetFieldValue<double>(9),
                            date = reader.GetFieldValue<string>(10),
                            created_at = reader.GetFieldValue<int>(11),
                            updated_at = reader.GetFieldValue<int>(12)
                        };
                        accounts.Add(t);
                    }
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public OrderDetailCustom Find(string id)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`,`orderid`, `store`, `order_nbr`, `product_id`, `product_sku`, `product_url`, `qty`, `price`, `total_discount`, `date`, `created_at`, `updated_at` FROM `order_detail_custom` WHERE `id` = @id";
                cmd.Parameters.AddWithValue("@id", id);
                var accounts = new List<OrderDetailCustom>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new OrderDetailCustom()
                        {
                            id = reader.GetFieldValue<int>(0),
                            orderid = reader.GetFieldValue<string>(1),
                            store = reader.GetFieldValue<string>(3),
                            order_nbr = reader.GetFieldValue<string>(3),
                            product_id = reader.GetFieldValue<string>(4),
                            product_sku = reader.GetFieldValue<string>(5),
                            product_url = reader.GetFieldValue<string>(6),
                            qty = reader.GetFieldValue<int>(7),
                            price = reader.GetFieldValue<double>(8),
                            total_discount = reader.GetFieldValue<double>(9),
                            date = reader.GetFieldValue<string>(10),
                            created_at = reader.GetFieldValue<int>(11),
                            updated_at = reader.GetFieldValue<int>(12)
                        };
                        accounts.Add(t);
                    }
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(OrderDetailCustom obj)
        {
            try
            {
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `order_detail_custom`(`orderid`, `store`, `order_nbr`, `product_id`, `product_sku`, `product_url`, `qty`, `price`, `total_discount`, `date`, `created_at`, `updated_at`) VALUES (@orderid,@store,@order_nbr,@product_id,@product_sku,@product_url,@qty,@price,@total_discount,@date,@created_at,@updated_at)";
                cmd.Parameters.AddWithValue("@orderid", obj.orderid);
                cmd.Parameters.AddWithValue("@store", obj.store);
                cmd.Parameters.AddWithValue("@order_nbr", obj.order_nbr);
                cmd.Parameters.AddWithValue("@product_id", obj.product_id);
                cmd.Parameters.AddWithValue("@product_sku", obj.product_sku);
                cmd.Parameters.AddWithValue("@product_url", obj.product_url);
                cmd.Parameters.AddWithValue("@qty", obj.qty);
                cmd.Parameters.AddWithValue("@price", obj.price);
                cmd.Parameters.AddWithValue("@total_discount", obj.total_discount);
                cmd.Parameters.AddWithValue("@date", obj.date);
                cmd.Parameters.AddWithValue("@created_at", obj.created_at);
                cmd.Parameters.AddWithValue("@updated_at", obj.updated_at);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(OrderDetailCustom obj)
        {
            try
            {
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `order_detail_custom` SET `orderid`=@orderid,`store`=@store,`order_nbr`=@order_nbr,`product_id`=@product_id,`product_sku`=@product_sku,`product_url`=@product_url,`qty`=@qty,`price`=@price,`total_discount`=@total_discount,`date`=@date,`updated_at`=@updated_at WHERE `id`=@id";
                cmd.Parameters.AddWithValue("@id", obj.id);
                cmd.Parameters.AddWithValue("@orderid", obj.orderid);
                cmd.Parameters.AddWithValue("@store", obj.store);
                cmd.Parameters.AddWithValue("@order_nbr", obj.order_nbr);
                cmd.Parameters.AddWithValue("@product_id", obj.product_id);
                cmd.Parameters.AddWithValue("@product_sku", obj.product_sku);
                cmd.Parameters.AddWithValue("@product_url", obj.product_url);
                cmd.Parameters.AddWithValue("@qty", obj.qty);
                cmd.Parameters.AddWithValue("@price", obj.price);
                cmd.Parameters.AddWithValue("@total_discount", obj.total_discount);
                cmd.Parameters.AddWithValue("@date", obj.date);
                cmd.Parameters.AddWithValue("@created_at", obj.created_at);
                cmd.Parameters.AddWithValue("@updated_at", obj.updated_at);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `order_detail_custom` WHERE `id`=@id";
                cmd.Parameters.AddWithValue("@id", id);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
    }
}
