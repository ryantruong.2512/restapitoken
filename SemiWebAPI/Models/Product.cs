﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class Product
    {
        public int id { get; set; }
        public string store { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string code { get; set; }
        public double base_cost { get; set; }
        public double est_profit { get; set; }
        public string sku { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }
}
