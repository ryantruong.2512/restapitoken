﻿using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class MySQLAccess: IDisposable
    {
        public MySqlConnection conn;
        public string strconn = "";
        public string log = "";
        public string log2 = "";

        public MySQLAccess(IMySQLSetting setting)
        {
            conn = new MySqlConnection(setting.ConnectionString);//"server=localhost;database=semi_adchannel;user=semi;password=Semi@123");
            log = DateTime.Now.ToString();
            conn.Open();

        }
        public void Dispose()
        {
            this.conn.Close();
        }
        public void Reset()
        {
            try
            {
                //try
                //{
                //    log2 = this.conn.Ping().ToString();
                //    this.conn.Clone();
                //    if (this.conn.State == System.Data.ConnectionState.Open)
                //    {
                //        this.conn.Close();
                //        //conn = new MySqlConnection(strconn);//"server=localhost;database=semi_adchannel;user=semi;password=Semi@123");
                //        this.conn.Open();
                //    }
                //}
                //catch(Exception ex)
                //{
                //    if(ex.Message == "Connection was killed")
                //    {
                //        conn = new MySqlConnection(strconn);
                //        this.conn.Open();
                //    }
                //}
                
                //string m = "";
                //try
                //{
                //    log2 = this.conn.Ping().ToString();
                //    if (!this.conn.Ping())
                //    {
                //        this.conn.Open();
                //    }
                //}
                //catch(Exception ex)
                //{
                //    m = ex.Message;
                //}
                //if (this.conn.State == System.Data.ConnectionState.Closed)
                //{
                    
                //}
            }
            catch(Exception ex)
            {
                log =  "Reset: "+ex.Message;
            }
        }
        public string CheckConn()
        {
            try
            {
                this.Reset();
                
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `fullname`, `status`, `roles`, `username`, `password`, `created_at`, `updated_at` FROM `users` WHERE 1";
                var accounts = new List<User>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new User()
                        {
                            Id = reader.GetFieldValue<int>(0).ToString(),
                            fullname = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            roles = reader.GetFieldValue<string>(3),
                            username = reader.GetFieldValue<string>(4),
                            password = reader.GetFieldValue<string>(5).ToString()
                        };
                        accounts.Add(t);
                    }
                return accounts.Count.ToString() + " " + log;
            }
            catch (Exception ex)
            {
                return ex.Message + " " + conn.State.ToString() + " "+log + " Log 2: " + log2;
            }
        }
    }
}
