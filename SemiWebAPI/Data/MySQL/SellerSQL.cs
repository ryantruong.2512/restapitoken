﻿using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class SellerSQL:MySQLAccess
    {
        public SellerSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }
        public List<Seller> FindAll()
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `code`, `name`, `status` FROM `sellers` WHERE 1";
                var accounts = new List<Seller>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Seller()
                        {
                            id = reader.GetFieldValue<int>(0),
                            code = reader.GetFieldValue<string>(1),
                            name = reader.GetFieldValue<string>(2),
                            status = reader.GetFieldValue<string>(3)
                        };
                        accounts.Add(t);
                    }
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public Seller Find(string id)
        {
            try
            {
                this.Reset();
                var accounts = new List<Seller>();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `store`, `title`, `url`, `code`, `base_cost`, `sku`, `est_profit` FROM `products` WHERE id=@id";
                cmd.Parameters.AddWithValue("@id", id);
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Seller()
                        {
                            id = reader.GetFieldValue<int>(0),
                            code = reader.GetFieldValue<string>(1),
                            name = reader.GetFieldValue<string>(2),
                            status = reader.GetFieldValue<string>(3)
                        };
                        accounts.Add(t);
                    }
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(Seller account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `sellers`(`code`, `name`, `status`) VALUES (@code,@name,@status)";
                cmd.Parameters.AddWithValue("@code", account.code);
                cmd.Parameters.AddWithValue("@name", account.name);
                cmd.Parameters.AddWithValue("@status", account.status);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(Seller account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `sellers` SET `code`=@code,`name`=@name,`status`=@status WHERE `id`= @id";
                cmd.Parameters.AddWithValue("@id", account.id);
                cmd.Parameters.AddWithValue("@code", account.code);
                cmd.Parameters.AddWithValue("@name", account.name);
                cmd.Parameters.AddWithValue("@status", account.status);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `sellers` WHERE `id` =@id";
                cmd.Parameters.AddWithValue("@id", id);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool DeleteAll()
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `sellers` WHERE 1";
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
    }
}
