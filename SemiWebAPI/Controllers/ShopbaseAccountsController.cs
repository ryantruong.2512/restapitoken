﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using SemiWebAPI.Services;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class ShopbaseAccountsController:Controller
    {
        ShopbaseAccountService _sbaccountsrv;
        public ShopbaseAccountsController(ShopbaseAccountService fbService)
        {
            _sbaccountsrv = fbService;
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_sbaccountsrv.Read());
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_sbaccountsrv.Find(id.ToString()));
        }


        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] Shopbase_Account obj)
        {
            try
            {
                return Ok(_sbaccountsrv.Create(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] Shopbase_Account obj)
        {
            try
            {
                return Ok(_sbaccountsrv.Update(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_sbaccountsrv.Delete(id));
        }
    }
}
