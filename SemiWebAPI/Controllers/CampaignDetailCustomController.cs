﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using SemiWebAPI.Services;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class CampaignDetailCustomController : Controller
    {
        CampaignDetailCustomService _campcustomsrv;
        public CampaignDetailCustomController(CampaignDetailCustomService campcustomsrv)
        {
            _campcustomsrv = campcustomsrv;
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_campcustomsrv.Read());
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_campcustomsrv.Find(id.ToString()));
        }

        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] CampaignDetailCustom obj)
        {
            try
            {
                obj.campaign_id = Common.BaseFunctions.ConvertDateTimeToMilliseconds(DateTime.UtcNow);
                obj.created_at = int.Parse(Common.BaseFunctions.ConvertDateTimeToSeconds(DateTime.UtcNow));
                obj.updated_at = int.Parse(Common.BaseFunctions.ConvertDateTimeToSeconds(DateTime.UtcNow));
                return Ok(_campcustomsrv.Create(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] CampaignDetailCustom obj)
        {
            try
            {
                return Ok(_campcustomsrv.Update(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_campcustomsrv.Delete(id));
        }
    }
}
