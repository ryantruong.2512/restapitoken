﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Services;
using SemiWebAPI.Models;
using SemiWebAPI.Provider;
namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class ProductsController: Controller
    {
        ProductService _productsrv;
        public ProductsController(ProductService fbService)
        {
            _productsrv = fbService;
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_productsrv.ReadAsync());
        }
        [HttpPost("import")]
        [Authorize(Roles = "administrator")]
        public ActionResult Import([FromBody] Product[] products)
        {
            var _count = _productsrv.Import(products.ToList());

            if (_count >=0)
            {

                return Ok(_count);
            }
            return Ok("Error!");
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_productsrv.Find(id.ToString()));
        }


        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] Product obj)
        {
            try
            {
                return Ok(_productsrv.Create(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] Product obj)
        {
            try
            {
                return Ok(_productsrv.Update(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_productsrv.Delete(id));
        }
    }
}
