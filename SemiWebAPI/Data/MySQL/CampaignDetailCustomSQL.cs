﻿//using MySql.Data.MySqlClient;
using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class CampaignDetailCustomSQL : MySQLAccess
    {
        public CampaignDetailCustomSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }
        public List<CampaignDetailCustom> FindAll()
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `campaign_id`, `campaign_name`, `spentofday`, `date`, `created_at`, `updated_at`, `account_id`, `account_name` FROM `campaign_detail_custom` WHERE 1";
                var accounts = new List<CampaignDetailCustom>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new CampaignDetailCustom()
                        {
                            campaign_id = reader.GetFieldValue<string>(0),
                            campaign_name = reader.GetFieldValue<string>(1),
                            spentofday = reader.GetFieldValue<double>(2),
                            date = reader.GetFieldValue<string>(3),
                            created_at = reader.GetFieldValue<int>(4),
                            updated_at = reader.GetFieldValue<int>(5),
                            account_id = reader.GetFieldValue<string>(6),
                            account_name = reader.GetFieldValue<string>(7)
                        };
                        accounts.Add(t);
                    }
                conn_tmp.Close();
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public CampaignDetailCustom Find(string campaign_id)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `campaign_id`, `campaign_name`, `spentofday`, `date`, `created_at`, `updated_at`, `account_id`, `account_name` FROM `campaign_detail_custom` WHERE `campaign_id`= @campaign_id";
                cmd.Parameters.AddWithValue("@campaign_id", campaign_id);
                var accounts = new List<CampaignDetailCustom>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new CampaignDetailCustom()
                        {
                            campaign_id = reader.GetFieldValue<string>(0),
                            campaign_name = reader.GetFieldValue<string>(1),
                            spentofday = reader.GetFieldValue<double>(2),
                            date = reader.GetFieldValue<string>(3),
                            created_at = reader.GetFieldValue<int>(4),
                            updated_at = reader.GetFieldValue<int>(5),
                            account_id = reader.GetFieldValue<string>(6),
                            account_name = reader.GetFieldValue<string>(7)
                        };
                        accounts.Add(t);
                    }
                conn_tmp.Close();
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(CampaignDetailCustom obj)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `campaign_detail_custom`(`campaign_id`, `campaign_name`, `spentofday`, `date`, `created_at`, `updated_at`, `account_id`, `account_name`) VALUES (@campaign_id, @campaign_name, @spentofday,@date,@created_at,@updated_at, @account_id, @account_name)";
                cmd.Parameters.AddWithValue("@campaign_id", obj.campaign_id);
                cmd.Parameters.AddWithValue("@campaign_name", obj.campaign_name);
                cmd.Parameters.AddWithValue("@spentofday", obj.spentofday);
                cmd.Parameters.AddWithValue("@date", obj.date);
                cmd.Parameters.AddWithValue("@created_at", obj.created_at);
                cmd.Parameters.AddWithValue("@updated_at", obj.updated_at);
                cmd.Parameters.AddWithValue("@account_id", obj.account_id);
                cmd.Parameters.AddWithValue("@account_name", obj.account_name);
                var res = cmd.ExecuteNonQuery();
                conn_tmp.Close();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(CampaignDetailCustom obj)
        {
            try
            {
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `campaign_detail_custom` SET `spentofday`=@spentofday,`date`=@date,`updated_at`=@updated_at WHERE `campaign_id` = @campaign_id and `date` = @date";
                cmd.Parameters.AddWithValue("@campaign_id", obj.campaign_id);
                cmd.Parameters.AddWithValue("@spentofday", obj.spentofday);
                cmd.Parameters.AddWithValue("@date", obj.date);
                cmd.Parameters.AddWithValue("@updated_at", obj.updated_at);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string campaign_id)
        {
            try
            {
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `campaign_detail_custom` WHERE `campaign_id`=@campaign_id";
                cmd.Parameters.AddWithValue("@campaign_id", campaign_id);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
    }
}
