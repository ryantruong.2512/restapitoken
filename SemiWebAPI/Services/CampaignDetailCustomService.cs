﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class CampaignDetailCustomService
    {
        CampaignDetailCustomSQL _campcustom;
        public CampaignDetailCustomService(IMySQLSetting setting)
        {
            _campcustom = new CampaignDetailCustomSQL(setting);
        }
        public CampaignDetailCustom Create(CampaignDetailCustom account)
        {
            _campcustom.Insert(account);
            return account;
        }
        public IList<CampaignDetailCustom> Read() =>
            _campcustom.FindAll();

        public CampaignDetailCustom Find(string id) =>
            _campcustom.Find(id);

        public CampaignDetailCustom Update(CampaignDetailCustom obj)
        {
            var result = _campcustom.Update(obj);
            return result?obj:null;
        }

        public string Delete(string id)
        {
            return _campcustom.Delete(id)?id:null;
            
        }

    }
}
