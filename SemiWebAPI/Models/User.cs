﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    [BsonIgnoreExtraElements]
    public class User
    {
        public string Id { get; set; }
        public string fullname { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string status { get; set; }
        public string roles { get; set; }
        public string seller { get; set; }
        public string team_members { get; set; }
        public string access_token { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime created_at { get; set; }
    }
}
