﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class Shopbase_Account
    {
        public string id { get; set; }
        public string store { get; set; }
        public string api_url { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string status { get; set; }
        public string log { get; set; }
        public string last_date_order { get; set; }
        public string last_date_crawl { get; set; }
    }
}
