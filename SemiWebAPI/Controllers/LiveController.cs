﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LiveController : Controller
    {
        [HttpGet("mysql")]
        public ActionResult CheckMysql()
        {
            return Ok();
        }
    }
}
