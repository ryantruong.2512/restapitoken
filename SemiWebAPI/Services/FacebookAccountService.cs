﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class FacebookAccountService
    {
        FacebookSQL _fbaccount;
        public FacebookAccountService(IMySQLSetting setting)
        {
            _fbaccount = new FacebookSQL(setting);
        }
        public Facebook_Account Create(Facebook_Account account)
        {
            _fbaccount.Insert(account);
            return account;
        }
        public IList<Facebook_Account> Read() =>
            _fbaccount.FindAll();

        public Facebook_Account Find(string id) =>
            _fbaccount.Find(id);

        public Facebook_Account Update(Facebook_Account obj)
        {
            var result = _fbaccount.Update(obj);
            return result?obj:null;
        }

        public string Delete(string id)
        {
            return _fbaccount.Delete(id)?id:null;
            
        }

    }
}
