﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class OrderDetailCustom
    {
        public int id { get; set; }
        public string orderid { get; set; }
        public string store { get; set; }
        public string order_nbr { get; set; }
        public string product_id { get; set; }
        public string product_sku { get; set; }
        public string product_url { get; set; }
        public int qty { get; set; }
        public double price { get; set; }
        public double total_discount { get; set; }
        public string date { get; set; }
        public int created_at { get; set; }
        public int updated_at { get; set; }
    }
}
