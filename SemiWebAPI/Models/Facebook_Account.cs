﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class Facebook_Account
    {
        public string id { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public string status { get; set; }
        public string log { get; set; }
        public string last_date_campaign { get; set; }
        public string last_date_crawl { get; set; }
    }
}
