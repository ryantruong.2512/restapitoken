﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class Reports
    {
        public class General
        {
            public string store { get; set; }
            public string code { get; set; }
            public string code_product { get; set; }
            public string seller { get; set; }
            public double adscost { get; set; }
            public int qty { get; set; }
            public double basecost { get; set; }
            public double estprofit { get; set; }
            public double roi { get; set; }
            public string fromdate { get; set; }
            public string todate { get; set; }
            public string thumbnail { get; set; }
            public double shipping { get; set; }
            public double discount { get; set; }
        }
        public class GeneralSummarySeller
        {
            public string seller { get; set; }
            public double adscost { get; set; }
            public int qty { get; set; }
            public double basecost { get; set; }
            public double estprofit { get; set; }
            public double roi { get; set; }
            public List<General> details { get; set; }
            public string fromdate { get; set; }
            public string todate { get; set; }
        }
        public class GeneralFacebookAdsDetail
        {
            //SELECT `campaign_id`, `campaign_name`, `spentofday`, `date`, `created_at`, `updated_at`, `account_id`, `account_name` FROM `campaign_detail` WHERE 1
            public string campaign_id { get; set; }
            public string campaign_name { get; set; }
            public double spentofday { get; set; }
            public string date { get; set; }
            public string account_id { get; set; }
            public string account_name { get; set; }
            public string fromdate { get; set; }
            public string todate { get; set; }
        }
        public class GeneralSalesDetail
        {
            //SELECT `orderid`, `store`, `order_nbr`, `product_id`, `product_sku`, `product_url`, `qty`, `price`, `total_discount`, `date`, `created_at`, `updated_at` FROM `order_detail` WHERE 1
            public string orderid { get; set; }
            public string store { get; set; }
            public string order_nbr { get; set; }
            public string product_id { get; set; }
            public string code { get; set; }
            public string product_code { get; set; }
            public int qty { get; set; }
            public string date { get; set; }
            public string fromdate { get; set; }
            public string todate { get; set; }
        }
        public class GeneralBaseCostDetail
        {
            //SELECT `orderid`, `store`, `order_nbr`, `product_id`, `product_sku`, `product_url`, `qty`, `price`, `total_discount`, `date`, `created_at`, `updated_at` FROM `order_detail` WHERE 1
            public string code_product { get; set; }
            public string name { get; set; }
            public int qty { get; set; }
            public double base_cost { get; set; }
            public double ads_cost { get; set; }
            public double total { get; set; }
            public double profit { get; set; }
        }
    }
}
