﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class ShopbaseAccountService
    {
        ShopbaseSQL _sbaccount;
        public ShopbaseAccountService(IMySQLSetting setting)
        {
            _sbaccount = new ShopbaseSQL(setting);
        }
        public Shopbase_Account Create(Shopbase_Account account)
        {
            _sbaccount.Insert(account);
            account.log = _sbaccount.log;
            return account;
        }
        public IList<Shopbase_Account> Read() =>
            _sbaccount.FindAll();

        public Shopbase_Account Find(string id) =>
            _sbaccount.Find(id);

        public Shopbase_Account Update(Shopbase_Account obj)
        {
            var result = _sbaccount.Update(obj);
            return result ? obj : null;
        }

        public string Delete(string id)
        {
            return _sbaccount.Delete(id) ? id : null;

        }
    }
}
