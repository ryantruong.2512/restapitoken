﻿using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class ProductSQL:MySQLAccess
    {
        public ProductSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }
        public List<Product> FindAllAsync()
        {
            try
            {
                var conn_tmp = conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `store`, `title`, `url`, `code`, `base_cost`, `sku`, `est_profit`,fromdate, todate FROM `products` WHERE 1 Order by `todate` DESC, `fromdate` DESC, `code` DESC";
                var accounts = new List<Product>();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var t = new Product()
                        {
                            id = reader.GetFieldValue<int>(0),
                            store = reader.GetFieldValue<string>(1),
                            name = reader.GetFieldValue<string>(2),
                            url = reader.GetFieldValue<string>(3),
                            code = reader.GetFieldValue<string>(4),
                            base_cost = reader.GetFieldValue<double>(5),
                            sku = reader.GetFieldValue<string>(6),
                            est_profit = reader.GetFieldValue<double>(7),
                            fromdate = reader.GetFieldValue<string>(8),
                            todate = reader.GetFieldValue<string>(9)
                        };
                        accounts.Add(t);
                    }
                    reader.Close();
                }
                
                conn.Close();
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public Product Find(string id)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `store`, `title`, `url`, `code`, `base_cost`, `sku`, `est_profit`, fromdate, todate FROM `products` WHERE id=@id Order by `todate` DESC, `fromdate` DESC, `code` DESC";
                var accounts = new List<Product>();
                cmd.Parameters.AddWithValue("@id", id);
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Product()
                        {
                            id = reader.GetFieldValue<int>(0),
                            store = reader.GetFieldValue<string>(1),
                            name = reader.GetFieldValue<string>(2),
                            url = reader.GetFieldValue<string>(3),
                            code = reader.GetFieldValue<string>(4),
                            base_cost = reader.GetFieldValue<double>(5),
                            sku = reader.GetFieldValue<string>(6),
                            est_profit = reader.GetFieldValue<double>(7),
                            fromdate = reader.GetFieldValue<string>(8),
                            todate = reader.GetFieldValue<string>(9)
                        };
                        accounts.Add(t);
                    }
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(Product account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `products`(`store`, `title`, `url`, `code`, `base_cost`, `sku`, `est_profit`, `fromdate`, `todate`) VALUES (@store,@title,@url,@code,@base_cost,@sku, @est_profit,@fromdate, @todate)";
                cmd.Parameters.AddWithValue("@store", account.store==null?"": account.store);
                cmd.Parameters.AddWithValue("@title", account.name == null ? "" : account.name);
                cmd.Parameters.AddWithValue("@url", account.url == null ? "" : account.url);
                cmd.Parameters.AddWithValue("@code", account.code == null ? "" : account.code);
                cmd.Parameters.AddWithValue("@sku", account.sku == null ? "" : account.sku);
                cmd.Parameters.AddWithValue("@base_cost", account.base_cost);
                cmd.Parameters.AddWithValue("@est_profit", account.est_profit);
                cmd.Parameters.AddWithValue("@fromdate", account.fromdate);
                cmd.Parameters.AddWithValue("@todate", account.todate);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(Product account)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `products` SET `store`=@store,`title`=@title,`url`=@url,`code`=@code,`base_cost`=@base_cost,`sku`=@sku, `est_profit` = @est_profit WHERE `id` = @id";
                cmd.Parameters.AddWithValue("@id", account.id);
                cmd.Parameters.AddWithValue("@store", account.store);
                cmd.Parameters.AddWithValue("@title", account.name);
                cmd.Parameters.AddWithValue("@url", account.url);
                cmd.Parameters.AddWithValue("@code", account.code);
                cmd.Parameters.AddWithValue("@base_cost", account.base_cost);
                cmd.Parameters.AddWithValue("@sku", account.sku);
                cmd.Parameters.AddWithValue("@est_profit", account.est_profit);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `products` WHERE `id` =@id";
                cmd.Parameters.AddWithValue("@id", id);
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool DeleteAll()
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `products` WHERE 1";
                var res = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
    }
}
