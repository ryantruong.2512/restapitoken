﻿using Microsoft.IdentityModel.Tokens;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SemiWebAPI.Provider
{
    public class AuthProvider
    {
        public string GenerateToken(User user)
        {
            string securityKey = "this_is_super_long_security_key_for_token_validation_project_2018_09_07$smesk.in";

            // symmetric security key
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            // signing credentials
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            //add claims

            var claims = new List<Claim>();
            string[] arr_roles = user.roles.Split(',');
            foreach(var role in arr_roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Trim()));
            }
            claims.Add(new Claim("username", user.username));
            claims.Add(new Claim("fullname", user.fullname));
            claims.Add(new Claim("roles", user.roles));
            claims.Add(new Claim("seller", user.seller)); 
            claims.Add(new Claim("team_members", user.team_members));
            // create token
            var token = new JwtSecurityToken(
                issuer: "smesk.in",
                audience: "readers",
                expires: DateTime.Now.AddMonths(12),
                signingCredentials: signingCredentials,
                claims: claims
            );

           
            // return token
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
