﻿using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class OrderTrackingService
    {
        public HttpClient Client { get; }

        public OrderTrackingService(HttpClient client)
        {
            Client = client;
        }

        public async Task<string> GetToken(string url, string username, string password)
        {
            try
            {
                var todoItemJson = new StringContent("{\"username\":\"" + username + "\",\"password\":\"" + password + "\"}",
                 Encoding.UTF8,
                 "application/json");

                using var httpResponse =
                    await Client.PostAsync(url, todoItemJson);

                httpResponse.EnsureSuccessStatusCode();
                string responseBody = await httpResponse.Content.ReadAsStringAsync();
                return responseBody;
            }
            catch
            {
                return null;
            }
        }
        public async Task<string> GetOrderID(string url, string token)
        {
            try
            {
                // GitHub API versioning
                //Client.DefaultRequestHeaders.Add("Authorization",
                //    "Bearer " + token);
                Client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Bearer", token);
                using var httpResponse =
                    await Client.GetAsync(url);

                httpResponse.EnsureSuccessStatusCode();
                string responseBody = await httpResponse.Content.ReadAsStringAsync();
                return responseBody;
                //MatchCollection matches = Regex.Matches(responseBody, "\"order_id\":(\\d+)");
                //if (matches.Count > 0)
                //{
                //    return matches[0].Groups[1].Value.ToString();
                //}
                //return "";
            }
            catch
            {
                return null;
            }
        }
        public async Task<string> CheckOrderShipment(string url, string token)
        {
            try
            {
                // GitHub API versioning
                //Client.DefaultRequestHeaders.Add("Authorization",
                //    "Bearer " + token);
                Client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Bearer", token);
                using var httpResponse =
                    await Client.GetAsync(url);

                httpResponse.EnsureSuccessStatusCode();
                string responseBody = await httpResponse.Content.ReadAsStringAsync();
                MatchCollection matches = Regex.Matches(responseBody, "\"track_number\":\"([^\"]+)\"");
                if (matches.Count > 0)
                {
                    return matches[0].Groups[1].Value.ToString();
                }
                return "";
            }
            catch
            {
                return null;
            }
        }
        public async Task<string> UpdateOrderShipment(string url, string token, OrderTracking order)
        {
            try
            {
                Client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue("Bearer", token);
                var todoItemJson = new StringContent("{\"items\": [{\"order_item_id\": "+order.order_item_id+", \"qty\": 1}],\"tracks\": [{\"track_number\": \""+order.tracking_number+"\", \"title\": \""+order.carrier+"\", \"carrier_code\": \""+order.carrier+"\"}]}",
                 Encoding.UTF8,
                 "application/json");

                using var httpResponse =
                    await Client.PostAsync(url, todoItemJson);

                httpResponse.EnsureSuccessStatusCode();
                string responseBody = await httpResponse.Content.ReadAsStringAsync();
                MatchCollection matches = Regex.Matches(responseBody, "\"track_number\":\"([^\"]+)\"");
                if (matches.Count > 0)
                {
                    return matches[0].Groups[1].Value.ToString();
                }
                return "";
            }
            catch
            {
                return null;
            }
        }
    }
}
