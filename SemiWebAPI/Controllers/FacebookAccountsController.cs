﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using SemiWebAPI.Services;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class FacebookAccountsController: Controller
    {
        FacebookAccountService _fbaccountsrv;
        public FacebookAccountsController(FacebookAccountService fbService)
        {
            _fbaccountsrv = fbService;
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_fbaccountsrv.Read());
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_fbaccountsrv.Find(id.ToString()));
        }


        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] Facebook_Account obj)
        {
            try
            {
                return Ok(_fbaccountsrv.Create(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] Facebook_Account obj)
        {
            try
            {
                return Ok(_fbaccountsrv.Update(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_fbaccountsrv.Delete(id));
        }
    }
}
