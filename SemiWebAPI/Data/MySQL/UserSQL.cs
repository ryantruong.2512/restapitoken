﻿using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Models;
namespace SemiWebAPI.Data.MySQL
{
    public class UserSQL : MySQLAccess
    {
        public UserSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }

        public List<User> FindAll()
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `fullname`, `status`, `roles`, `username`, `password`, `seller`, `team_members`,`created_at`, `updated_at` FROM `users` WHERE 1";
                var accounts = new List<User>();

                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new User()
                        {
                            Id = reader.GetFieldValue<int>(0).ToString(),
                            fullname = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            roles = reader.GetFieldValue<string>(3),
                            username = reader.GetFieldValue<string>(4),
                            password = reader.GetFieldValue<string>(5).ToString(),
                            seller = reader.GetFieldValue<string>(6).ToString(),
                            team_members = reader.GetFieldValue<string>(7)
                        };
                        accounts.Add(t);
                    }
                conn_tmp.Close();
                return accounts;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public User Find(string id)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"SELECT `id`, `fullname`, `status`, `roles`, `username`, `password`, `created_at`, `updated_at` FROM `users` WHERE `id` = @id";
                var accounts = new List<User>();
                cmd.Parameters.AddWithValue("@id", id);
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new User()
                        {
                            Id = reader.GetFieldValue<int>(0).ToString(),
                            fullname = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            roles = reader.GetFieldValue<string>(3),
                            username = reader.GetFieldValue<string>(4),
                            password = reader.GetFieldValue<string>(5).ToString()
                        };
                        accounts.Add(t);
                    }
                conn_tmp.Close();
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public User FindByAccessTokenAsync(string access_token)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"call proc_GetUserByAccessToken(@access_token)";
                var accounts = new List<User>();
                cmd.Parameters.AddWithValue("@access_token", access_token);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var t = new User()
                        {
                            Id = reader.GetFieldValue<int>(0).ToString(),
                            fullname = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            roles = reader.GetFieldValue<string>(3),
                            username = reader.GetFieldValue<string>(4),
                            password = reader.GetFieldValue<string>(5),
                            seller = reader.GetFieldValue<string>(6),
                            team_members = reader.GetFieldValue<string>(7)
                        };
                        accounts.Add(t);
                    }
                    reader.Close();
                }
                conn_tmp.Close();
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public User Find(string username, string password)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"call proc_getusers(@username, @password)";
                
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@password", password);
                var accounts = new List<User>();
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new User()
                        {
                            Id = reader.GetFieldValue<int>(0).ToString(),
                            fullname = reader.GetFieldValue<string>(1),
                            status = reader.GetFieldValue<string>(2),
                            roles = reader.GetFieldValue<string>(3),
                            username = reader.GetFieldValue<string>(4),
                            password = reader.GetFieldValue<string>(5),
                            seller = reader.GetFieldValue<string>(6),
                            team_members = reader.GetFieldValue<string>(7)
                        };
                        accounts.Add(t);
                    }
                conn_tmp.Close();
                return accounts[0];
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public bool Insert(User account)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `users`(`fullname`, `status`, `roles`, `username`, `password`, `seller`,`team_members`) VALUES (@fullname,@status,@roles,@username,@password, @seller,@team_members)";
                cmd.Parameters.AddWithValue("@fullname", account.fullname);
                cmd.Parameters.AddWithValue("@status", account.status);
                cmd.Parameters.AddWithValue("@roles", account.roles);
                cmd.Parameters.AddWithValue("@username", account.username);
                cmd.Parameters.AddWithValue("@password", account.password);
                cmd.Parameters.AddWithValue("@seller", account.seller);
                cmd.Parameters.AddWithValue("@team_members", account.team_members);
                var res = cmd.ExecuteNonQuery();
                conn_tmp.Close();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Update(User account)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `users` SET `fullname`=@fullname,`status`=@status,`roles`=@roles,`password`=@password, `team_members`=@team_members WHERE `username`=@username";
                cmd.Parameters.AddWithValue("@fullname", account.fullname);
                cmd.Parameters.AddWithValue("@status", account.status);
                cmd.Parameters.AddWithValue("@roles", account.roles);
                cmd.Parameters.AddWithValue("@username", account.username);
                cmd.Parameters.AddWithValue("@password", account.password);
                cmd.Parameters.AddWithValue("@access_token", account.access_token);
                cmd.Parameters.AddWithValue("@team_members", account.team_members);
                var res = cmd.ExecuteNonQuery();
                conn_tmp.Close();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool UpdateToken(User account)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `users` SET `access_token`=@access_token WHERE `username`=@username";
                cmd.Parameters.AddWithValue("@username", account.username);
                cmd.Parameters.AddWithValue("@access_token", account.access_token);
                var res = cmd.ExecuteNonQuery();
                conn_tmp.Close();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
        public bool Delete(string id)
        {
            try
            {
                var conn_tmp = this.conn.Clone();
                conn_tmp.Open();
                var cmd = conn_tmp.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"DELETE FROM `users` WHERE `username` =@username";
                cmd.Parameters.AddWithValue("@username", id);
                var res = cmd.ExecuteNonQuery();
                conn_tmp.Close();
                return true;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return false;
            }
        }
    }
}
