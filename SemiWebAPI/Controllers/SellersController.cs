﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Services;
using SemiWebAPI.Models;
using SemiWebAPI.Provider;
namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class SellersController : Controller
    {
        SellerService _sellersrv;
        public SellersController(SellerService fbService)
        {
            _sellersrv = fbService;
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_sellersrv.Read());
        }
        [HttpPost("import")]
        [Authorize(Roles = "administrator")]
        public ActionResult Import([FromBody] Seller[] Sellers)
        {
            var _count = _sellersrv.Import(Sellers.ToList());
            if (_count >=0)
            {
                return Ok(_count);
            }
            return Ok("Error!");
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_sellersrv.Find(id.ToString()));
        }


        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] Seller obj)
        {
            try
            {
                return Ok(_sellersrv.Create(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] Seller obj)
        {
            try
            {
                return Ok(_sellersrv.Update(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_sellersrv.Delete(id));
        }
    }
}
