﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class ProductService
    {
        ProductSQL _psql;
        public ProductService(IMySQLSetting setting)
        {
            _psql = new ProductSQL(setting);
        }
        public Product Create(Product account)
        {
            _psql.Insert(account);
            return account;
        }
        public int Import(List<Product> products)
        {
            _psql.DeleteAll();
            int count = 0;
            foreach(var product in products)
            {
                if (!string.IsNullOrEmpty(product.code)) {
                    product.fromdate = DateTime.Parse(product.fromdate).ToString("yyyy-MM-dd");
                    product.todate = DateTime.Parse(product.todate).ToString("yyyy-MM-dd");
                    if (_psql.Insert(product))
                        count++;
                }
               
            }
            return count;
        }
        public IList<Product> ReadAsync() =>
            _psql.FindAllAsync();

        public Product Find(string id) =>
            _psql.Find(id);

        public Product Update(Product obj)
        {
            var result = _psql.Update(obj);
            return result ? obj : null;
        }

        public string Delete(string id)
        {
            return _psql.Delete(id) ? id : null;

        }
    }
}
