﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Services;
using SemiWebAPI.Models;
using SemiWebAPI.Provider;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class OrderTrackingController : Controller
    {
        StoresService _storesSvc;
        OrderTrackingService _ordertrackingSvc;
        public OrderTrackingController(StoresService storeService, OrderTrackingService orderTrackingService)
        {
            _storesSvc = storeService;
            _ordertrackingSvc = orderTrackingService;
        }

        //[Authorize(Roles = "administrator")]
        [HttpPost("token")]
        public async Task<ActionResult> UpdateTrackingAsync([FromBody] OrderTracking order)
        {
            try
            {
                var token = await _ordertrackingSvc.GetToken("https://jerrygear.supertool.us/rest/V1/integration/admin/token", "phucadmin03", "Hoangphuc2512@");
                return Ok(token);
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpPost("find-order")]
        public async Task<ActionResult> FindOrderAsync([FromBody] OrderTracking order)
        {
            try
            {
                var token = await _ordertrackingSvc.GetOrderID("https://jerrygear.supertool.us/rest/V1/orders?searchCriteria[filter_groups][0][filters][0][field]=increment_id&searchCriteria[filter_groups][0][filters][0][value]="+ order.order_name, order.store_token);
                return Ok(token);
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpPost("check-order-shipment")]
        public async Task<ActionResult> CheckOrderShipmentAsync([FromBody] OrderTracking order)
        {
            try
            {
                var token = await _ordertrackingSvc.CheckOrderShipment("https://jerrygear.supertool.us/rest/V1/shipments?searchCriteria[filter_groups][0][filters][0][field]=order_id&searchCriteria[filter_groups][0][filters][0][value]=" + order.order_id, order.store_token);
                return Ok(token);
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpPost("update-order-shipment")]
        public async Task<ActionResult> UpdateOrderShipmentAsync([FromBody] OrderTracking order)
        {
            try
            {
                var token = await _ordertrackingSvc.CheckOrderShipment("https://jerrygear.supertool.us/rest/V1/shipments?searchCriteria[filter_groups][0][filters][0][field]=order_id&searchCriteria[filter_groups][0][filters][0][value]=" + order.order_id, order.store_token);
                return Ok(token);
            }
            catch
            {
                return Ok("Error!");
            }
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator,staff")]
        public ActionResult Index()
        {
            return Ok(_storesSvc.Read());
        }
        [HttpGet("{id:length(24)}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_storesSvc.Find(id.ToString()));
        }

      
        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] Store user)
        {
            try
            {
                return Ok(_storesSvc.Create(user));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id:length(24)}")]
        public ActionResult Edit([FromBody] Store store)
        {
            try
            {
                return Ok(_storesSvc.Update(store));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id:length(24)}")]
        public ActionResult Delete(string id)
        {
            return Ok(_storesSvc.Delete(id));
        }
    }
}
