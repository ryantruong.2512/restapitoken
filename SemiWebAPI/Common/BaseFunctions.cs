﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Common
{
    public class BaseFunctions
    {
        public static DateTime MillisecondsStampToDateTime(double Milliseconds)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(Milliseconds).ToLocalTime();
            return dtDateTime;
        }
        public static string ConvertDateTimeToMilliseconds(DateTime datetime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long ms = (long)(datetime - epoch).TotalMilliseconds;
            return ms.ToString();
        }
        public static DateTime SecondsToDateTime(double Second)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Second).ToLocalTime();
            return dtDateTime;
        }
        public static string ConvertDateTimeToSeconds(DateTime datetime)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long ms = (long)(datetime - epoch).TotalSeconds;
            return ms.ToString();
        }
    }
}
