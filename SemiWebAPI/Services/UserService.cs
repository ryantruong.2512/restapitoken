﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Models;
using SemiWebAPI.Data.MySQL;

namespace SemiWebAPI.Services
{
    public class UserService
    {
        private UserSQL _users;
        public string log { get; set; }

        public UserService(IMySQLSetting settings)
        {
            _users = new UserSQL(settings);
        }

        public User Login(string username, string password) { 
            var user = _users.Find(username, password);
            log = _users.log;
            return user;
        }
        public string Check()
        {
            return  _users.CheckConn();
        }
        public User Create(User user)
        {
            _users.Insert(user);
            return user;
        }

        public IList<User> Read() =>
            _users.FindAll().ToList();

        public User Find(string id) =>
            _users.Find(id);
        public User FindByAccessTokenAsync(string access_token) =>
            _users.FindByAccessTokenAsync(access_token);

        public User Update(User user){
            var result = _users.Update(user);
            if(result)
            {
                return user;
            }
            return null;
        }
        public User UpdateToken(User user)
        {
            var result = _users.UpdateToken(user);
            if (result)
            {
                return user;
            }
            return null;
        }

        public string Delete(string id)
        {
            var result = _users.Delete(id);
            if(result)
            {
                return id;
            }
            return null;
        }
           
    }
}
