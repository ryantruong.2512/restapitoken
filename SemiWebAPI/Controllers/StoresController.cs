﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Services;
using SemiWebAPI.Models;
using SemiWebAPI.Provider;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class StoresController : Controller
    {
        StoresService _storesSvc;
        public StoresController(StoresService storeService)
        {
            _storesSvc = storeService;
        }
      
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_storesSvc.Read());
        }
        [HttpGet("{id:length(24)}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_storesSvc.Find(id.ToString()));
        }

      
        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] Store user)
        {
            try
            {
                return Ok(_storesSvc.Create(user));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id:length(24)}")]
        public ActionResult Edit([FromBody] Store store)
        {
            try
            {
                return Ok(_storesSvc.Update(store));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id:length(24)}")]
        public ActionResult Delete(string id)
        {
            return Ok(_storesSvc.Delete(id));
        }
    }
}
