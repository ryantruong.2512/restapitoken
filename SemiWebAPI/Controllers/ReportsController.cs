﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Services;
using SemiWebAPI.Models;
using SemiWebAPI.Provider;
using SemiWebAPI.Common;
namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class ReportsController:Controller
    {
        ReportsService _reSvc;
        public ReportsController(ReportsService reSvc)
        {
            _reSvc = reSvc;
        }
        [HttpGet("general")]
        [Authorize(Roles = "administrator,leader,staff")]
        public ActionResult Details(string seller, string fromdate, string todate)
        {
            return Ok(_reSvc.General01(seller, fromdate, todate));
        }
        [Authorize(Roles = "administrator,leader,staff")]
        [HttpGet("generalfacebookadsdetails")]
        public ActionResult GeneralFacebookAdsDetails(string code, string fromdate, string todate)
        {
            return Ok(_reSvc.GeneralFacebookAdsDetails(code, fromdate, todate));
        }
        [HttpGet("generalsalesdetails")]
        [Authorize(Roles = "administrator,leader,staff")]
        public ActionResult GeneralSalesDetails(string code, string fromdate, string todate)
        {
            return Ok(_reSvc.GeneralSalesDetails(code, fromdate, todate));
        }
        [HttpGet("generalbasecostdetails")]
        [Authorize(Roles = "administrator,leader,staff")]
        public ActionResult GeneralBaseCostDetails(string seller, string fromdate, string todate)
        {
            return Ok(_reSvc.GeneralBaseCostDetails(seller, fromdate, todate));
        }
    }

}
