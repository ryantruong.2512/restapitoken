﻿using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Services
{
    public class ReportsService
    {
        ReportsSQL _psql;
        public ReportsService(IMySQLSetting setting)
        {
            _psql = new ReportsSQL(setting);
        }

        public IList<Reports.General> General01(string seller, string fromdate, string todate)
        {

            var _fromdate = String.IsNullOrEmpty(fromdate) ? DateTime.Now.ToString("yyyy-MM-dd"):DateTime.Parse(fromdate).ToString("yyyy-MM-dd");
            var _todate = String.IsNullOrEmpty(todate) ? DateTime.Now.ToString("yyyy-MM-dd"):DateTime.Parse(todate).ToString("yyyy-MM-dd");
            var _seller = String.IsNullOrEmpty(seller) ? "%" : seller;
            //var ge =  _psql.General(_seller, _fromdate, _todate);
            //var list_by_sellers = new List<Reports.GeneralSummarySeller>();
           
            //foreach(var g in ge)
            //{
            //    if(list_by_sellers.FindIndex(x=>x.seller == g.seller) >=0)
            //    {

            //    } else
            //    {
            //        var obj = new Reports.GeneralSummarySeller();
            //        obj.seller = g.seller;
            //        list_by_sellers.Add(obj);
            //    }
            //}
            //for(int i=0; i<list_by_sellers.Count; i++)
            //{
            //    list_by_sellers[i].details = new List<Reports.General>();
            //    foreach (var g in ge)
            //    {
            //        if(list_by_sellers[i].seller == g.seller)
            //        {
            //            list_by_sellers[i].qty += g.qty;
            //            list_by_sellers[i].adscost += g.adscost;
            //            list_by_sellers[i].estprofit += g.estprofit;
            //            list_by_sellers[i].estprofit += g.estprofit;
            //            list_by_sellers[i].details.Add(g);
            //        }
            //    }
            //}
            //for (int i = 0; i < list_by_sellers.Count; i++)
            //{
            //    //tính ROI.
            //}
            return _psql.General(_seller, _fromdate, _todate);
        }
        public IList<Reports.GeneralFacebookAdsDetail> GeneralFacebookAdsDetails(string code, string fromdate, string todate)
        {

            var _fromdate = String.IsNullOrEmpty(fromdate) ? DateTime.Now.ToString("yyyy-MM-dd") : DateTime.Parse(fromdate).ToString("yyyy-MM-dd");
            var _todate = String.IsNullOrEmpty(todate) ? DateTime.Now.ToString("yyyy-MM-dd") : DateTime.Parse(todate).ToString("yyyy-MM-dd");
            var _code = String.IsNullOrEmpty(code) ? "%" : code;
            return _psql.GeneralFacebookAdsDetails(_code, _fromdate, _todate);
        }

        public IList<Reports.GeneralSalesDetail> GeneralSalesDetails(string code, string fromdate, string todate)
        {

            var _fromdate = String.IsNullOrEmpty(fromdate) ? DateTime.Now.ToString("yyyy-MM-dd") : DateTime.Parse(fromdate).ToString("yyyy-MM-dd");
            var _todate = String.IsNullOrEmpty(todate) ? DateTime.Now.ToString("yyyy-MM-dd") : DateTime.Parse(todate).ToString("yyyy-MM-dd");
            var _code = String.IsNullOrEmpty(code) ? "%" : code;
            return _psql.GeneralSalesDetails(_code, _fromdate, _todate);
        }
        public IList<Reports.GeneralBaseCostDetail> GeneralBaseCostDetails(string seller, string fromdate, string todate)
        {

            var _fromdate = String.IsNullOrEmpty(fromdate) ? DateTime.Now.ToString("yyyy-MM-dd") : DateTime.Parse(fromdate).ToString("yyyy-MM-dd");
            var _todate = String.IsNullOrEmpty(todate) ? DateTime.Now.ToString("yyyy-MM-dd") : DateTime.Parse(todate).ToString("yyyy-MM-dd");
            var _code = String.IsNullOrEmpty(seller) ? "%" : seller;
            return _psql.GeneralBasecost(_code, _fromdate, _todate);
        }
    }
}
