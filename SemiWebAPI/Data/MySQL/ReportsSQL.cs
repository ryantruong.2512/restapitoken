﻿using MySqlConnector;
using SemiWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Data.MySQL
{
    public class ReportsSQL:MySQLAccess
    {
        public ReportsSQL(IMySQLSetting _mysql) : base(_mysql)
        {

        }
        public List<Reports.General> General(string seller, string fromdate, string todate)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"call proc_ReportSemi(@seller, @fromdate, @todate)";
                cmd.Parameters.AddWithValue("@seller", seller);
                cmd.Parameters.AddWithValue("@fromdate", fromdate);
                cmd.Parameters.AddWithValue("@todate", todate);
                List<Reports.General> list = new List<Reports.General>();
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Reports.General()
                        {
                            store = reader.GetFieldValue<string>(0),
                            code = reader.GetFieldValue<string>(1),
                            code_product = reader.GetFieldValue<string>(2),
                            seller = reader.GetFieldValue<string>(3),
                            qty = int.Parse( reader.GetFieldValue<object>(4).ToString()),
                            adscost = double.Parse(reader.GetFieldValue<object>(5).ToString()),
                            estprofit = reader.GetFieldValue<double>(6),
                            basecost = reader.GetFieldValue<double>(7),
                            roi = reader.GetFieldValue<double>(8),
                            fromdate = reader.GetFieldValue<string>(9),
                            todate = reader.GetFieldValue<string>(10),
                            thumbnail = reader.GetFieldValue<string>(11),
                            shipping = reader.GetFieldValue<double>(12),
                            discount = reader.GetFieldValue<double>(13)
                        };
                        list.Add(t);
                    }
                return list;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public List<Reports.GeneralFacebookAdsDetail> GeneralFacebookAdsDetails(string code, string fromdate, string todate)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"call proc_ReportFacebookAdsDetails(@code, @fromdate, @todate)";
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@fromdate", fromdate);
                cmd.Parameters.AddWithValue("@todate", todate);
                List<Reports.GeneralFacebookAdsDetail> list = new List<Reports.GeneralFacebookAdsDetail>();
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        //SELECT `campaign_id`, `campaign_name`, `spentofday`, `date`, `created_at`, `updated_at`, `account_id`, `account_name` FROM `campaign_detail_custom` WHERE 1
                        var t = new Reports.GeneralFacebookAdsDetail()
                        {
                            campaign_id = reader.GetFieldValue<string>(0),
                            campaign_name = reader.GetFieldValue<string>(1),
                            spentofday = reader.GetFieldValue<double>(2),
                            date = reader.GetFieldValue<string>(3),
                            account_id = reader.GetFieldValue<string>(4),
                            account_name = reader.GetFieldValue<string>(5),
                            fromdate = reader.GetFieldValue<string>(6),
                            todate = reader.GetFieldValue<string>(7)
                        };
                        list.Add(t);
                    }
                return list;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public List<Reports.GeneralSalesDetail> GeneralSalesDetails(string code, string fromdate, string todate)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"call proc_ReportSalesDetails(@code, @fromdate, @todate)";
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@fromdate", fromdate);
                cmd.Parameters.AddWithValue("@todate", todate);
                List<Reports.GeneralSalesDetail> list = new List<Reports.GeneralSalesDetail>();
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Reports.GeneralSalesDetail()
                        {
                            orderid = reader.GetFieldValue<string>(0),
                            store = reader.GetFieldValue<string>(1),
                            order_nbr = reader.GetFieldValue<string>(2),
                            product_id = reader.GetFieldValue<string>(3),
                            code = reader.GetFieldValue<string>(4),
                            product_code = reader.GetFieldValue<string>(5),
                            date = reader.GetFieldValue<string>(6),
                            qty = reader.GetFieldValue<int>(7),
                            fromdate = reader.GetFieldValue<string>(8),
                            todate = reader.GetFieldValue<string>(9)
                        };
                        list.Add(t);
                    }
                return list;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
        public List<Reports.GeneralBaseCostDetail> GeneralBasecost(string seller, string fromdate, string todate)
        {
            try
            {
                this.Reset();
                var cmd = this.conn.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"call proc_ReportBaseCost(@seller, @fromdate, @todate)";//call proc_ReportBaseCost('%', '2021-03-09','2021-03-09')
                cmd.Parameters.AddWithValue("@seller", seller);
                cmd.Parameters.AddWithValue("@fromdate", fromdate);
                cmd.Parameters.AddWithValue("@todate", todate);
                List<Reports.GeneralBaseCostDetail> list = new List<Reports.GeneralBaseCostDetail>();
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                    {
                        var t = new Reports.GeneralBaseCostDetail()
                        {
                            code_product = reader.GetFieldValue<string>(0),
                            name = reader.GetFieldValue<string>(1),
                            qty = reader.GetFieldValue<int>(2),
                            base_cost = reader.GetFieldValue<double>(3),
                            total = reader.GetFieldValue<double>(4),
                            profit = reader.GetFieldValue<double>(5),
                        };
                        list.Add(t);
                    }
                return list;
            }
            catch (Exception ex)
            {
                log = ex.Message;
                return null;
            }
        }
    }
}
