﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SemiWebAPI.Data.MySQL;
using SemiWebAPI.Models;
using SemiWebAPI.Services;
using System.Text.RegularExpressions;

namespace SemiWebAPI.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class OrderDetailCustomController : Controller
    {
        OrderDetailCustomService _campcustomsrv;
        public OrderDetailCustomController(OrderDetailCustomService campcustomsrv)
        {
            _campcustomsrv = campcustomsrv;
        }
        // GET: UserController
        [HttpGet]
        [Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            return Ok(_campcustomsrv.Read());
        }
        [HttpGet("{id}")]
        [Authorize(Roles = "administrator")]
        public ActionResult Details(string id)
        {
            return Ok(_campcustomsrv.Find(id.ToString()));
        }

        [Authorize(Roles = "administrator")]
        [HttpPost]
        public ActionResult Create([FromBody] OrderDetailCustom obj)
        {
            try
            {
                obj.orderid = obj.order_nbr;
                obj.product_id = obj.product_url;
                obj.product_sku = obj.product_url;
                MatchCollection matches = Regex.Matches(obj.product_url, "([a-zA-Z][a-zA-Z]*\\d\\d\\d+[a-zA-Z][a-zA-Z])");
                if (matches.Count > 0)
                {
                    string code_tmp = matches[0].Groups[1].Value.ToString();
                    obj.product_sku = code_tmp;
                }
                obj.created_at = int.Parse(Common.BaseFunctions.ConvertDateTimeToSeconds(DateTime.UtcNow));
                obj.updated_at = int.Parse(Common.BaseFunctions.ConvertDateTimeToSeconds(DateTime.UtcNow));
                return Ok(_campcustomsrv.Create(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }

        [Authorize(Roles = "administrator")]
        [HttpPost("{id}")]
        public ActionResult Edit([FromBody] OrderDetailCustom obj)
        {
            try
            {
                MatchCollection matches = Regex.Matches(obj.product_url, "([a-zA-Z][a-zA-Z]*\\d\\d\\d+[a-zA-Z][a-zA-Z])");
                if (matches.Count > 0)
                {
                    string code_tmp = matches[0].Groups[1].Value.ToString();
                    obj.product_sku = code_tmp.Substring(code_tmp.ToString().Length - 2, 2);
                }
                obj.created_at = int.Parse(Common.BaseFunctions.ConvertDateTimeToSeconds(DateTime.UtcNow));
                obj.updated_at = int.Parse(Common.BaseFunctions.ConvertDateTimeToSeconds(DateTime.UtcNow));
                return Ok(_campcustomsrv.Update(obj));
            }
            catch
            {
                return Ok("Error!");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            return Ok(_campcustomsrv.Delete(id));
        }
    }
}
