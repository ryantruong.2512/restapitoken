﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class Seller
    {
        public int id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string status { get; set; }
    }
}
