﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class CampaignDetailCustom
    {
        public string campaign_id { get; set; }
        public string campaign_name { get; set; }
        public double spentofday { get; set; }
        public string date { get; set; }
        public int created_at { get; set; }
        public int updated_at { get; set; }
        public string account_id { get; set; }
        public string account_name { get; set; }
    }
}
