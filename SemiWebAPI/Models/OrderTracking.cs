﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemiWebAPI.Models
{
    public class OrderTracking
    {
        public string store_id { get; set; }
        public string store_token { get; set; }
        public string order_name { get; set; }
        public string order_id { get; set; }
        public string order_item_id { get; set; }
        public string carrier { get; set; }
        public string company_name { get; set; }
        public string tracking_number { get; set; }
    }
}
